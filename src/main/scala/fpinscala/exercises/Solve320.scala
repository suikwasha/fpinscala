package fpinscala.exercises

import fpinscala.datastructures._

object Solve320 extends App {

  println(List.flatMap(List(1, 2, 3))(i => List(i, i)))
}
