package fpinscala.exercises

import fpinscala.datastructures._

object Solve318 extends App {
  println(List.map(List(1, 2, 3, 4, 5))(_ + 1))
}
