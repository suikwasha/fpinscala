package fpinscala.exercises

import fpinscala.datastructures._

object Solve32 extends App {

  def tail[A](list: List[A]): List[A] = list match {
    case Cons(_, tail) => tail
    case Nil => throw new Exception()
  }

  println(tail(List(1,2,3,4)))
}

