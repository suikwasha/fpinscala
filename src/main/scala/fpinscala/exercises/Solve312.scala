package fpinscala.exercises

import fpinscala.datastructures._

object Solve312 extends App {

  def reverse[A](xs: List[A]): List[A] = List.foldLeft[A, List[A]](xs, Nil)((a, b) => Cons(b, a))

  println(reverse(List(1, 2, 3, 4)))
}
