package fpinscala.exercises

import fpinscala.datastructures._

object Solve315 extends App {

  def flatten[A](ass: List[List[A]]): List[A] =
    List.foldRight[List[A], List[A]](ass, Nil)(List.foldRight(_, _)(Cons(_, _)))

  println(flatten(List(List(1, 2, 3, 4), List(5, 6, 7, 8), List(9, 10, 11))))
}
