package fpinscala.exercises

import fpinscala.datastructures._

object Solve36 extends App {

  def init[A](l: List[A]): List[A] = l match {
    case Cons(_, Nil) => Nil
    case Cons(h, t) => Cons(h, init(t))
  }

  println(init(List(1,2,3,4,5)))
}

