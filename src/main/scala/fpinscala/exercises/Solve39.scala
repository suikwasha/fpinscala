package fpinscala.exercises

import fpinscala.datastructures._

object Solve39 extends App {

  def length[A](as: List[A]): Int =
    List.foldRight(as, 0)((_, l) => l + 1)


  println(length(List(1, 2, 3, 4)))
}
