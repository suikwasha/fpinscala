package fpinscala.exercises

import fpinscala.errorhandling._
import Option._

object Solve42 extends App {

  def variance(xs: Seq[Double]): Option[Double] =
    mean(xs).flatMap(m => mean(xs.map(x => Math.pow(x - m, 2))))
}
