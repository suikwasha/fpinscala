package fpinscala.exercises

import fpinscala.datastructures._

object Solve34 extends App {

  def drop[A](l: List[A], n: Int): List[A] = if (n <= 0) {
    l
  } else {
    l match {
      case Cons(_, tail) => drop(tail, n - 1)
      case Nil => throw new Exception()
    }
  }

  println(drop(List(1,2,3,4,5), 2))
}

