package fpinscala.exercises

import fpinscala.datastructures._

object Solve317 extends App {

  def mkString(as: List[Double]): List[String] =
    List.foldRight[Double, List[String]](as, Nil)((a, b) => Cons(a.toString, b))

  println(mkString(List(1, 2, 3, 4, 5)))
}
