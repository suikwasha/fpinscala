package fpinscala.exercises

import fpinscala.datastructures._

object Solve324 extends App {

  def init[A](xs: List[A]): List[List[A]] = xs match {
    case Cons(_, t) => Cons(xs, init(t))
    case Nil => Nil
  }

  def check[A](as: List[A], bs: List[A]): Boolean = (as, bs) match {
    case (Cons(a, at), Cons(b, bt)) if a == b => check(at, bt)
    case (_, Nil) => true
    case _ => false
  }

  def hasSubsequence[A](sup: List[A], sub: List[A]): Boolean =
    List.foldRight(List.map(init(sup))(check(_, sub)), false)(_ || _)

  val sup = List(1, 2, 3, 4)

  val sub1 = List(2, 3)

  val sub2 = List(1, 3)

  println(hasSubsequence(sup, sub1))
  println(hasSubsequence(sup, sub2))
}
