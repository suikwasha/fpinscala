package fpinscala.exercises

import fpinscala.errorhandling._

object Solve47 extends App {

  def sequence[E, A](es: List[Either[E, A]]): Either[E, List[A]] = es match {
    case h :: t => h.flatMap { (value: A) =>
      sequence(t).map(value :: _)
    }
    case Nil => Right(List())
  }

  def traverse[E, A, B](as: List[A])(f: A => Either[E, B]): Either[E, List[B]] = as match {
    case h :: t => f(h).flatMap { value => traverse(t)(f) }
    case Nil => Right(List())
  }
}
