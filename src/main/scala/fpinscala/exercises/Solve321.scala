package fpinscala.exercises

import fpinscala.datastructures._

object Solve321 extends App {

  def filter2[A](as: List[A])(f: A => Boolean): List[A] =
    List.flatMap(as)(a => if (f(a)) Nil else List(a))

  def isOdd(n: Int): Boolean = n % 2 != 0

  val numbers = List(1, 2, 3, 4, 5, 6, 7, 8, 9, 10)

  println(filter2(numbers)(isOdd))
}
