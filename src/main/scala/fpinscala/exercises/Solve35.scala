package fpinscala.exercises

import fpinscala.datastructures._

object Solve35 extends App {

  def dropWhile[A](l: List[A], cond: A => Boolean): List[A] = l match {
    case Cons(x, tail) if cond(x) => tail
    case Cons(x, tail) => dropWhile(tail, cond)
    case Nil => throw new Exception()
  }

  println(dropWhile[Int](List(1,2,3,4,5), _ == 2))
}

