package fpinscala.exercises

import fpinscala.errorhandling._

object Solve44 extends App {

  def sequence[A](a: List[Option[A]]): Option[List[A]] = a match {
    case Nil => Some(List())
    case head :: tail => head.flatMap(h => sequence(tail).map(h :: _))
  }


}
