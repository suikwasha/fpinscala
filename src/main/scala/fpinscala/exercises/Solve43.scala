package fpinscala.exercises

import fpinscala.errorhandling._

object Solve43 extends App {

  def map2[A, B, C](a: Option[A], b: Option[B])(f: (A, B) => C): Option[C] = {
    a.flatMap { v1 => b.map { v2 =>
        f(v1, v2)
      }
    }
  }
}
