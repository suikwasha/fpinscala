package fpinscala.exercises

import fpinscala.datastructures._

object Solve314 extends App {

  def appendRight[A](as: List[A], bs: List[A]): List[A] = List.foldRight(as, bs)(Cons(_, _))

  def appendLeft[A](as: List[A], bs: List[A]): List[A] =
    List.foldLeft(List.foldLeft[A, List[A]](as, Nil)((a, b) => Cons(b, a)), bs)((a, b) => Cons(b, a))

  println(appendRight(List(1, 2, 3), List(4, 5, 6)))
  println(appendLeft(List(1, 2, 3), List(4, 5, 6)))
}
