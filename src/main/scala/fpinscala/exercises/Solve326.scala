package fpinscala.exercises

import fpinscala.datastructures._

object Solve326 extends App {

  def maximum(t: Tree[Int]): Int = t match {
    case Leaf(n) => n
    case Branch(left, right) => Math.max(maximum(left), maximum(right))
  }

  val tree = Branch(
    Leaf(1),
    Branch(
      Leaf(2),
      Leaf(3)
    )
  )

  println(maximum(tree))

}
