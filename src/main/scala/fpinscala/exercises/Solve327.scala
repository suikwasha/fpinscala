package fpinscala.exercises

import fpinscala.datastructures._

object Solve327 extends App {

  def depth[A](t: Tree[A], value: A): Int = {

    def traverse(cur: Tree[A])(implicit depth: Int = 0): Int = cur match {
      case Leaf(v) if v == value => 1
      case Branch(left, right) => traverse(left) + traverse(right) + 1
      case _ => 0
    }

    traverse(t)
  }

  val tree = Branch(
    Leaf(1),
    Branch(
      Branch(
        Leaf(4),
        Leaf(5)
      ),
      Leaf(3)
    )
  )

  println(depth(tree, 5))

}
