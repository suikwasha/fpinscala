package fpinscala.exercises

import fpinscala.datastructures._

object Solve316 extends App {

  def addone(as: List[Int]): List[Int] =
    List.foldRight[Int, List[Int]](as, Nil)((a, b) => Cons(a + 1, b))

  println(addone(List(1, 2, 3, 4, 5)))

}
