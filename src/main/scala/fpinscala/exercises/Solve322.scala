package fpinscala.exercises

import fpinscala.datastructures._

object Solve322 extends App {

  def zip(as: List[Int], bs: List[Int]): List[Int] = (as, bs) match {
    case (Cons(a, atail), Cons(b, btail)) => Cons(a + b, zip(atail, btail))
    case (_, _) => Nil
  }

  println(zip(List(1, 2, 3), List(4, 5, 6)))
}
