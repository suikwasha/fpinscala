package fpinscala.exercises

import fpinscala.datastructures._

object Solve319 extends App {

  def filter[A](as: List[A])(f: A => Boolean): List[A] =
    List.foldRight[A, List[A]](as, Nil) { (a, b) =>
      if(f(a)) b else Cons(a, b)
    }

  def isOdd(n: Int): Boolean = n % 2 != 0

  val numbers = List(1, 2, 3, 4, 5, 6, 7, 8, 9, 10)

  println(filter(numbers)(isOdd))
}
