package fpinscala.exercises

import fpinscala.datastructures._

object Solve325 extends App {

  def size[A](tree: Tree[A]): Int = tree match {
    case l: Leaf[A] => 1
    case Branch(left, right) => size(left) + size(right) + 1
  }

  val tree = Branch(
    Leaf(1),
    Branch(
      Leaf(1),
      Leaf(1)
    )
  )

  println(size(tree))
}
