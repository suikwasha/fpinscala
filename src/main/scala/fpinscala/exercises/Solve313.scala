package fpinscala.exercises

import fpinscala.datastructures._

object Solve313 extends App {

  def foldRight[A, B](xs: List[A], z: B)(f: (B, A) => B): B =
    List.foldLeft[A, B](List.reverse(xs), z)(f)

  def p[A](xs: List[A]): String = foldRight(xs, "_")("(f " + _ + ", " + _ +")")

  println(p(List(1, 2, 3, 4)))
}
