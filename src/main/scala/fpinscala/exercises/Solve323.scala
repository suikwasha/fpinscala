package fpinscala.exercises

import fpinscala.datastructures._

object Solve323 extends App {

  def zipWith[A, B, C](as: List[A], bs: List[B])(f: (A, B) => C): List[C] = (as, bs) match {
    case (Cons(a, at), Cons(b, bt)) => Cons(f(a, b), zipWith(at, bt)(f))
    case _ => Nil
  }

  println(zipWith(List(1, 2, 3), List(3, 4, 5))(_ + _))
}
