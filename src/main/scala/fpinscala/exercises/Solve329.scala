package fpinscala.exercises

import fpinscala.datastructures._

object Solve329 extends App {

  def fold[A, B](t: Tree[A])(f: A => B)(g: (B, B) => B): B = t match {
    case Leaf(v) => f(v)
    case Branch(l, r) => g(fold(l)(f)(g), fold(r)(f)(g))
  }

  def size[A](t: Tree[A]): Int = fold(t)(_ => 1)(1 + _ + _)

  def maximum(t: Tree[Int]): Int = fold(t)(identity)(Math.max)

  def depth[A](t: Tree[A]): Int = fold(t)(_ => 0)((l, r) => Math.max(l, r) + 1)

  def map[A, B](t: Tree[A])(f: A => B): Tree[B] = fold[A, Tree[B]](t)(a => Leaf(f(a)))(Branch.apply)

  val tree = Branch(
    Leaf(1),
    Branch(
      Branch(
        Leaf(4),
        Leaf(5)
      ),
      Leaf(3)
    )
  )

  println(size(tree))
  println(maximum(tree))
  println(depth(tree))
  println(map(tree)(_ * 2))
}
