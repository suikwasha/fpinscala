package fpinscala.exercises

object Solve45 extends App {

  def traverse[A, B](a: List[A])(f: A => Option[B]): Option[List[B]] = a match {
    case Nil => Some(List())
    case head :: tail => f(head).flatMap(r => traverse(tail)(f).map(r :: _))
  }
}
