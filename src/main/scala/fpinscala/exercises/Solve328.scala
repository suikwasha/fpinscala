package fpinscala.exercises

import fpinscala.datastructures._

object Solve328 extends App {

  def map[A, B](t: Tree[A])(f: A => B): Tree[B] = t match {
    case Leaf(v) => Leaf(f(v))
    case Branch(left, right) => Branch(map(left)(f), map(right)(f))
  }

  val tree = Branch(
    Leaf(1),
    Branch(
      Branch(
        Leaf(4),
        Leaf(5)
      ),
      Leaf(3)
    )
  )

  println(map(tree)(_ * 2))
}
