package fpinscala.exercises

import fpinscala.datastructures._

object Solve311 extends App {

  def sum(ns: List[Int]): Int = List.foldLeft(ns, 0)(_ + _)

  def product(ns: List[Double]): Double = List.foldLeft(ns, 1.0)(_ + _)

  println(sum(List(1, 2, 3, 4)))

  println(product(List(1.0, 2.0, 3.0, 4.0)))
}

