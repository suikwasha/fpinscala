package fpinscala.datastructures

sealed trait List[+A]
case object Nil extends List[Nothing]
case class Cons[+A](head: A, tail: List[A]) extends List[A]

object List {
  def sum(ints: List[Int]): Int = ints match {
    case Nil => 0
    case Cons(x, xs) => x + sum(xs)
  }

  def product(ds: List[Double]): Double = ds match {
    case Nil => 1.0
    case Cons(0.0, _) => 0.0
    case Cons(x, xs) => x * product(xs)
  }

  def apply[A](as: A*): List[A] = 
    if (as.isEmpty) {
       Nil
    } else {
       Cons(as.head, apply(as.tail: _*))
    }

  def append[A](a1: List[A], a2: List[A]): List[A] = {
    a1 match {
      case Nil => a2
      case Cons(h, t) => Cons(h, append(t, a2))
    }
  }

  def foldRight[A, B](as: List[A], z: B)(f: (A, B) => B): B = as match {
    case Nil => z
    case Cons(x, xs) => f(x, foldRight(xs, z)(f))
  }

  def foldLeft[A, B](as: List[A], z: B)(f: (B, A) => B): B = as match {
    case Nil => z
    case Cons(x, xs) => foldLeft(xs, f(z, x))(f)
  }

  def map[A, B](as: List[A])(f: A => B): List[B] =
    List.foldRight[A, List[B]](as, Nil)((a, b) => Cons(f(a), b))

  def flatMap[A, B](as: List[A])(f: A => List[B]): List[B] =
    List.foldRight[A, List[B]](as, Nil) { (a, b) =>
      List.foldRight[B, List[B]](f(a), b) { (c, d) =>
        Cons(c, d)
      }
    }

  def reverse[A](xs: List[A]): List[A] = List.foldLeft[A, List[A]](xs, Nil)((a, b) => Cons(b, a))

  def sum2(ns: List[Int]) = foldRight(ns, 0)(_ + _)

  def product2(ns: List[Double]) = foldRight(ns, 1.0)(_ * _)
}
